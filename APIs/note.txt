How to use the APIs with Ex Nihilo:

The following APIs needed to be added to this folder, then added to the Eclipse Build Path (version numbers are in the build.properties file):
* IC2 Experimental: http://jenkins.ic2.player.to/job/IC2_experimental/
* Forestry: found at http://jenkins.ic2.player.to/job/Forestry/
* AE2: found at http://ae2.ae-mod.info/Downloads/
* Waila: found at http://minecraft.curseforge.com/mc-mods/waila/

The following mods need to be placed in the mods folder in your run directory (defaulted to outside your ex-nihilo folder):
* CCC(1.0.0) and NEI(1.0.1): found at http://chickenbones.net/Files/New_Versions/links.php.

(1) = The following line needs to be added to your VM arguments for your run configurations: -Dfml.coreMods.load=appeng.transformer.AppEngCore